var mongoose = require('mongoose'); 

var userSchema = mongoose.Schema({
    auth_token: String,
    auth_token_secret: String,
    profileData: {
        name: String,
        username: String,
        startDate: Number,
        email: String, 
        profilePicture: String,
        profileBackground: String, 
        location: {
            longitude: String,
            latitude: String,
        },
        available: Boolean,
    },
    friends: [
        /*
            {
                name: String,
                username: String,
                startDate: Number,
                email: String, 
                profilePicture: String,
                profileBackground: String, 
                location: {
                    longitude: String,
                    latitude: String,
                },
                available: Boolean,
            }
        */
    ],
    meetups: [
        /*
        {
            guid: String, 
            participants: [{
                name: String,
                username: String,
                startDate: Number,
                email: String, 
                profilePicture: String,
                profileBackground: String, 
                location: {
                    longitude: String,
                    latitude: String,
                },
                available: Boolean,
            }], 
            date: Number, 
            remindMeIn: Number, //Days, in MS
            title: String, 
            location: {
                name: String, 
                longitude: String, 
                latitude: String, 
            }, 
            attended: Boolean, 
        }
    */
    ],
    messages: [
        /*
        {
            from: {
                name: String, 
                Username: String, 
                profile_picture: String, 
            }, 
            to: [
                {
                    name: String, 
                    Username: String, 
                    profile_picture: String, 
                } 
            ], 
            text: String, 
            read: Boolean, 
        }
    */
    ],
});



var User = mongoose.model("User", userSchema);
module.exports = User;
