//TODO: Handle errors
//Add Status codes to all responses. 

module.exports = function(User) {
    var mongoose = require('mongoose');
    var User = User;
    var config = require('../config.json');
    var Twit = require('twit');

    var routes = {
        index: function(req, res) {
            res.render('index');
        },

        /*
            @route: POST
            @accepts: auth_token
            @purpose: Set local instance to be authenticate
        */

        authenticate: function(req, res) {

            var userPost = req.body;

            /*
                {
                "auth_token": "TOKEN_HERE", 
                "email": "EMAIL_HERE"
                }
            */

            //IF auth_token and user_email are unique, create new user account. 
            //IF not, send back the current user data
            User.find({
                auth_token: userPost.auth_token
            }, function(err, users) {
                if (!err) {
                    if (users.length != 0) {
                        console.log("Logged In!");

                        var foundUser = users[0];

                        //Remove ID and __V
                        foundUser['__v'] = undefined;
                        foundUser['_id'] = undefined;

                        res.send(foundUser);

                    } else {
                        console.log("Starting to save...");
                        routes.utils.getNewUser(userPost, function(newUser) {
                            newUser.save(function(err) {
                                if (err) {
                                    console.log("Error saving");
                                    throw err;
                                } else {
                                    console.log("New user saved!");

                                    //Delete irrelevant portions
                                    newUser['_v'] = undefined;
                                    newUser['_id'] = undefined;
                                    res.send(newUser);
                                }
                            });

                        });
                    }
                }
            });
        },

        /*
            @route: POST
            @accepts: auth_token, 
            @purpose: Gets all meetups for a certain user. 
            @returns: **Status code (TODO)**, meetups array.
        */

        getMeetups: function(req, res) {

            var auth_token = req.params.auth_token;

            User.find({
                auth_token: auth_token
            }, function(err, users) {
                if (err) {
                    throw err;
                } else {
                    if (users.length > 0) {
                        console.log("Sent!");
                        res.send(users[0].meetups);
                    } else {
                        console.log("Sent!");
                        res.send([]);
                    }
                }

            });

        },

        /*
            @route: POST
            @accepts: auth_token, Meetup data
            @purpose: Generates new meetup from POSTed meetup. 
            @returns: Status code   
        */

        newMeetup: function(req, res) {
            res.send({});
        },

        /*
            @route: POST
            @accepts: auth_token, Meetup data
            @purpose: Updates current meetup with certain GUID
            @returns: Status code   
        */
        updateMeetup: function(req, res) {
            res.send({});
        },

        /*
            @route: POST
            @accepts: local auth_token, friend object 
            @purpose: Adds to friends list
            @returns: Status code and timestamp 
        */
        connectWithUser: function(req, res) {
            res.send({});
        },

        /*
            @route: POST
            @accepts: Auth_Token, Search Query
            @purpose: Look up friends by Search query, which is by Username and Real name
            @returns: Status code and timestamp 
        */
        searchUsers: function(req, res){

            var query = req.params.user; 
            
            //Get Partial String Match Regex of the query...
            var regexquery = new RegExp("^" + query + "", "i");       
            
            User.find({"$or": [{"profileData.name": {"$regex": regexquery}}, {"profileData.username": {"$regex": regexquery}}]}, function(err, users){
                if(err){
                    throw err; 
                }else{
                    var userProfileData = []; 

                    users.forEach(function(user){
                        userProfileData.push(user.profileData); 
                    }); 

                    res.send(userProfileData); 
                }
            })

        }, 

        utils: {
            getNewUser: function(user, done) {

                console.log(user);
                console.log("Logging into twitter");

                var Twitter = new Twit({
                    consumer_key: config.twitter.consumer_key,
                    consumer_secret: config.twitter.consumer_secret,
                    access_token: user.auth_token,
                    access_token_secret: user.auth_token_secret,
                });

                Twitter.get("account/verify_credentials", {}, function(err, reply) {

                    var newUser = new User({
                        auth_token: user.auth_token,
                        auth_token_secret: user.auth_token_secret,
                        name: reply.name,
                        username: reply.screen_name,
                        profileData: {
                            name: reply.name,
                            username: reply.screen_name,
                            email: user.email,
                            startDate: new Date().getTime(),
                            profilePicture: reply.profile_image_url.replace("normal", "bigger"),
                            profileBackground: reply.profile_banner_url + "/mobile", 
                        },
                        meetups: [],
                        messages: [],
                    });

                    done(newUser);
                });
            },
        }
    };

    return routes;
}
