/*****************************
 * Author: Akhil Acharya
 * Project: Lets Meet
 * Started: 3/11/14
 ******************************/


var express = require('express');

//Configuration Data
var config = require('./config.json');


//DB
var mongoose = require('mongoose');
var db_url = config.db.base_url + config.db.username + ":" + config.db.password + config.db.path; 
mongoose.connect(db_url); 

var db = mongoose.connection; 

db.on('error', console.error.bind(console, 'Connection error'));

db.once('open', function callback() {
    console.log("Connected to remote MongoDB");
}); 


//App
var app = module.exports = express.createServer();

//Models
var User = require('./models/User.js');


//Controllers
var Controller = require('./controllers/MeetController.js')(User);

app.configure(function() {
    app.use(express.bodyParser());
    app.use(express.cookieParser());
    app.use(express.session({
        secret: "I'm actually Sri Lankan"
    }));
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(app.router);
    app.use(express.static(__dirname + '/public'));
});



app.configure('development', function() {
    app.use(express.errorHandler({
        dumpExceptions: true,
        showStack: true
    }));
});

app.configure('production', function() {
    app.use(express.errorHandler());
});

// Routes
app.get('/', Controller.index);
app.post('/api/auth', Controller.authenticate); 

//Authenticated: 
app.get('/api/:auth_token/getMeetups', Controller.getMeetups); 
app.post('/api/:auth_token/newMeetup', Controller.newMeetup); 

//Public
app.get("/api/search/:user", Controller.searchUsers); 

//Listen in
app.listen(8081, function() {
    console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
    console.log("Currently in testing mode");
});
